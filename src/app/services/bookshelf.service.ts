import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Book } from '../types/book'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookshelfService {

  private bookshelfUrl = 'http://localhost:8080/book';  // URL to web api

  constructor(private http: HttpClient) { }

  getBooksByTitleOrDescriptionPart(titleOrDescriptionPart: string): Observable<Book[]> {
    return this.http.get<Book[]>(`${this.bookshelfUrl}/findByTitleOrDescriptionPart?titleOrDescriptionPart=${titleOrDescriptionPart}`);
  }

  getDummyBook(): Observable<Book> {
    return this.http.get<Book>(`${this.bookshelfUrl}/dummy`);
  }
}
