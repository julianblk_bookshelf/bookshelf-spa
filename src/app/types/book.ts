import { Author } from "./author";

export class Book {
    isbn: string;
    title: string;
    subtitle: string;
    published: string;
    publisher: string;
    quantity: number;
    instock: boolean;
    authors: Author[];
}