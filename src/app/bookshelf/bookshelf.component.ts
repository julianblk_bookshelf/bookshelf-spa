import { Component, OnInit } from '@angular/core';
import { Book } from '../types/book';
import { BookshelfService } from '../services/bookshelf.service'

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrls: ['./bookshelf.component.css']
})
export class BookshelfComponent implements OnInit {

  books: Book[] = [];
  titleOrDescriptionPart: string;

  constructor(private bookShelfSvc : BookshelfService) { }

  ngOnInit() { }

  search(): void {

    if (!this.titleOrDescriptionPart || this.titleOrDescriptionPart.trim() === '') {
      this.titleOrDescriptionPart = '';
      this.books = [];
      return;
    }

    this.titleOrDescriptionPart = this.titleOrDescriptionPart.trim();
    this.bookShelfSvc.getBooksByTitleOrDescriptionPart(this.titleOrDescriptionPart)
      .subscribe(books => this.books = books);
  }

  getDummyBook(): void {
    this.bookShelfSvc.getDummyBook()
      .subscribe(book => this.books.push(book));
  }
}
